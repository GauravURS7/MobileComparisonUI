/**
 * @author Gaurav Kumar
 * created on 10.04.2017
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.productTOI')
      .controller('ProductComparePageCtrl', 
                  ['$scope','ProductComapreService','toastr',function ProductComparePageCtrl($scope,ProductComapreService,toastr) {
      
    /*
    * This service called a method to get the complete list of product attributes from the server.
    */
    ProductComapreService.productAttributeList()
        .then(function(response){
        
        $scope.productAttributeDisplayNameSummary = []; 
        $scope.productAttributeDisplayNameGeneral = []; 
        $scope.productAttributeDisplayNameStorage = []; 
          
          for(var i = 0; i < 7; i++ ) {
            
            if(i < 3) {
                $scope.productAttributeDisplayNameSummary.push(response.productAttr[i].productAttributeDisplayName);
                
            } else if(i < 5) {
                $scope.productAttributeDisplayNameGeneral.push(response.productAttr[i].productAttributeDisplayName);
                
            } else {
                
                 $scope.productAttributeDisplayNameStorage.push(response.productAttr[i].productAttributeDisplayName);
                
            }
          }
          
      });//productAttributeList()
      
    /*
    * This service called a method to get the complete list of product type from the server.
    */
    ProductComapreService.productTypeList()
        .then(function(response){
          
          $scope.productTypeName = []
          
          for(var i in response.productType) {
              
              $scope.productTypeName.push(response.productType[i].productTypeDisplayName);
          }
        console.log(response.productType);
      });//productTypeList()
      
    $scope.isLoading = true;
   
    /*
    * This method is called to search the product type via product type name from first search box.
    * 
    *@param productTypeName
    */
    $scope.searchProduct = function(productTypeName){
        
        /*
        * This service called a method to search the product type via product type name from the server.
        * 
        *@param productTypeName
        */       
        ProductComapreService.searchProductTypeByName(productTypeName)
            .then(function(response){
            
            var index = 0;
             
            $scope.productType = response.productType[index].productTypeDisplayName;
            $scope.productPrice = response.productType[index].productTypePrice;
            $scope.isLoading = false;
            
            var productTypeId = response.productType[index].productTypeId;
            
            /*
            * This service called a method to search the product details via product type id from the server.
            *
            * @param productTypeId
            */
            ProductComapreService.getProductDetailsByProductTypeId(productTypeId)
                .then(function(response){
                
                var productAttribute = [];
                
                for(var i in response.productDetail) {
                    productAttribute.push(response.productDetail[i].productAttribute);
                }
                
                $scope.productAttributeSummary = []; 
                $scope.productAttributeGeneral = []; 
                $scope.productAttributeStorage = []; 
                
                for(var i = 0; i < 7; i++ ) {
                    
                    if(i < 3) {
                        
                        $scope.productAttributeSummary.push(productAttribute[i].productAttributeValue);
                    
                    } else if(i < 5) {
                        
                        $scope.productAttributeGeneral.push(productAttribute[i].productAttributeValue);
                    
                    } else {
                        
                        $scope.productAttributeStorage.push(productAttribute[i].productAttributeValue);
                    }
                
                }       
                            
            },function(error){
            
                toastr.error(error);
            
            },function(notify){
        
                toastr.success(notify);
            
            });//getProductDetailsByProductTypeId(-)
        
        },function(error){
            
            toastr.error(error);
          
        },function(notify){
          
            toastr.success(notify);
            
        });//searchProductTypeByName(-)
        
    }//searchProduct()
    
    /*
    * This method is called to clear all the detail displayed on the page, searched by first search box
    */
    $scope.clearFirstSearchProduct = function() {
        $scope.isLoading = true;
        $scope.productAttributeSummary = {}; 
        $scope.productAttributeGeneral = {}; 
        $scope.productAttributeStorage = {};
        $scope.mobName = null;
    } //clearFirstSearchProduct()    
    
    $scope.isLoading1 = true;

    /*
    * This method called to search the product type via product type name from second search box.
    * 
    *@param productTypeName
    */
    $scope.searchProduct1 = function(productTypeName){

        /*
        * This service called a method to search the product type via product type name from the server.
        * 
        * @param productTypeName
        */        
        ProductComapreService.searchProductTypeByName(productTypeName)
            .then(function(response){
           
            var index = 0;
            
            $scope.productType1 = response.productType[index].productTypeDisplayName;
            $scope.productPrice1 = response.productType[index].productTypePrice;
            $scope.isLoading1 = false;
            
            var productTypeId = response.productType[index].productTypeId;

            /*
            * This service called a method to search the product details via product type id from the server.
            * @param productTypeId
            */            
            ProductComapreService.getProductDetailsByProductTypeId(productTypeId)
                .then(function(response){
            
                var productAttribute1 = [];
                
                for(var i in response.productDetail) {
                    productAttribute1.push(response.productDetail[i].productAttribute);
                }
                
                $scope.productAttribute1Summary = []; 
                $scope.productAttribute1General = []; 
                $scope.productAttribute1Storage = []; 
                
                for(var i = 0; i < 7; i++ ) {
                    
                    if(i < 3) {
                        
                        $scope.productAttribute1Summary.push(productAttribute1[i].productAttributeValue);
                    
                    } else if(i < 5) {
                        
                        $scope.productAttribute1General.push(productAttribute1[i].productAttributeValue);
                    
                    } else {
                        
                        $scope.productAttribute1Storage.push(productAttribute1[i].productAttributeValue);
                    }
                
                }       

            
        },function(error){
                
                toastr.error(error);
            
            },function(notify){
                
                toastr.success(notify);
                
            });//getProductDetailsByProductTypeId(-)
            
        },function(error){
            
            toastr.error(error);
            
        },function(notify){
            
           toastr.success(notify);
        
        });//searchProductTypeByName(-)
        
    }//searchProduct1()

    /*
    * This method is called to clear all the detail displayed on the page, searched by second search box
    */    
    $scope.clearSecondSearchProduct = function() {
        $scope.isLoading1 = true;
        $scope.productAttribute1Summary = {}; 
        $scope.productAttribute1General = {}; 
        $scope.productAttribute1Storage = {};
        $scope.mobName1 = null;
    }//clearSecondSearchProduct()
        
    $scope.isLoading2 = true;

    /*
    * This method called to search the product type via product type name from third search box.
    *
    *@param productTypeName
    */
    $scope.searchProduct2 = function(productTypeName){

        /*
        * This service called a method to search the product type via product type name from the server.
        *
        * @param productTypeName
        */        
        ProductComapreService.searchProductTypeByName(productTypeName)
            .then(function(response){
            
            var index = 0;
            
            $scope.productType2 = response.productType[index].productTypeDisplayName;
            $scope.productPrice2 = response.productType[index].productTypePrice;
            $scope.isLoading2 = false;
            
            var productTypeId = response.productType[index].productTypeId;
            
            /*
            * This service called a method to search the product details via product type id from the server.
            *
            * @param productTypeId
            */            
            ProductComapreService.getProductDetailsByProductTypeId(productTypeId)
                .then(function(response){
            
                var productAttribute2 = [];
                
                for(var i in response.productDetail) {
                    productAttribute2.push(response.productDetail[i].productAttribute);
                }
                
                $scope.productAttribute2Summary = []; 
                $scope.productAttribute2General = []; 
                $scope.productAttribute2Storage = []; 
                
                for(var i = 0; i < 7; i++ ) {
                    
                    if(i < 3) {
                        
                        $scope.productAttribute2Summary.push(productAttribute2[i].productAttributeValue);
                    
                    } else if(i < 5) {
                        
                        $scope.productAttribute2General.push(productAttribute2[i].productAttributeValue);
                    
                    } else {
                        
                        $scope.productAttribute2Storage.push(productAttribute2[i].productAttributeValue);
                    }
                
                }       

            
            },function(error){
            
                toastr.error(error);
            
            },function(notify){
            
                toastr.success(notify);
            
            });//getProductDetailsByProductTypeId(-)
            
        },function(error){
            
            toastr.error(error);
            
        },function(notify){
            
            toastr.success(notify);
            
        });//searchProductTypeByName(-)
 
    }//searchProduct2()

    /*
    * This method is called to clear all the detail displayed on the page, searched by third search box
    */    
    $scope.clearThirdSearchProduct = function() {
        $scope.isLoading2 = true;
        $scope.productAttribute2Summary = {}; 
        $scope.productAttribute2General = {}; 
        $scope.productAttribute2Storage = {};
        $scope.mobName2 = null;
    }//clearThirdSearchProduct()
        
    $scope.isLoading3 = true;

    /*
    * This method called to search the product type via product type name from fourth search box.
    *
    *@param productTypeName
    */                      
    $scope.searchProduct3 = function(productTypeName){
  
        /*
        * This service called a method to search the product type via product type name from the server.
        *
        * @param productTypeName
        */        
        ProductComapreService.searchProductTypeByName(productTypeName)
            .then(function(response){
            
            var index = 0;
           
            $scope.productType3 = response.productType[index].productTypeDisplayName;
            $scope.productPrice3 = response.productType[index].productTypePrice;
            $scope.isLoading3 = false;
            
            var productTypeId = response.productType[index].productTypeId;
            
            /*
            * This service called a method to search the product details via product type id from the server.
            *
            * @param productTypeId
            */            
            ProductComapreService.getProductDetailsByProductTypeId(productTypeId)
                .then(function(response){
            
                var productAttribute3 = [];
                
                for(var i in response.productDetail) {
                    productAttribute3.push(response.productDetail[i].productAttribute);
                }
                
                $scope.productAttribute3Summary = []; 
                $scope.productAttribute3General = []; 
                $scope.productAttribute3Storage = []; 
                
                for(var i = 0; i < 7; i++ ) {
                    
                    if(i < 3) {
                        
                        $scope.productAttribute3Summary.push(productAttribute3[i].productAttributeValue);
                    
                    } else if(i < 5) {
                        
                        $scope.productAttribute3General.push(productAttribute3[i].productAttributeValue);
                    
                    } else {
                        
                        $scope.productAttribute3Storage.push(productAttribute3[i].productAttributeValue);
                    }
                
                }       

                
        },function(error){
                
                toastr.error(error);
                
            },function(notify){
                
                toastr.success(notify);
                
            });//getProductDetailsByProductTypeId(-)
            
        },function(error){
            
            toastr.error(error);
            
        },function(notify){
            
                toastr.success(notify);
        });//searchProductTypeByName(-)
 
    }//searchProduct3()

    /*
    * This method is called to clear all the detail displayed on the page, searched by fourth search box
    */    
    $scope.clearFourthSearchProduct = function() {
        $scope.isLoading3 = true;
        $scope.productAttribute3Summary = {}; 
        $scope.productAttribute3General = {}; 
        $scope.productAttribute3Storage = {};
        $scope.mobName3 = null;
    }//clearFourthSearchProduct()
    
  }]);

})();
